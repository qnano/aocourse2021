"""
Example for PC in right corner

Setup right corner:
    Camera 1: Regular camera
    Camera 2: SH sensor
"""

from camera.ueye_camera import uEyeCamera
from pyueye import ueye

import numpy as np
import matplotlib.pyplot as plt

SH_Sensor_Index = 4
Camera_Index = 1

def grabframes(nframes, cameraIndex=0):
    with uEyeCamera(device_id=cameraIndex) as cam:
        cam.set_colormode(ueye.IS_CM_MONO8)#IS_CM_MONO8)
        w=1280
        h=1024
        cam.set_aoi(0,0, w, h)
        
        cam.alloc(buffer_count=10)
        cam.set_exposure(0.5)
        cam.capture_video(True)
    
        imgs = np.zeros((nframes,h,w),dtype=np.uint8)
        acquired=0
        # For some reason, the IDS cameras seem to be overexposed on the first frames (ignoring exposure time?). 
        # So best to discard some frames and then use the last one
        while acquired<nframes:
            frame = cam.grab_frame()
            if frame is not None:
                imgs[acquired]=frame
                acquired+=1
            
    
        cam.stop_video()
    
    return imgs

if __name__ == "__main__":
    from dm.okotech.dm import OkoDM

    # Use "with" blocks so the hardware doesn't get locked when you press ctrl-c    
    with OkoDM(dmtype=0) as dm:
        print(f"Deformable mirror with {len(dm)} actuators")
        dm.setActuators(np.random.uniform(-1,1,size=len(dm)))
        
        plt.figure()
        img=grabframes(5, 1)
        plt.imshow(img[-1])
        
        plt.figure()
        img=grabframes(5, 4)
        plt.imshow(img[-1])
        
            